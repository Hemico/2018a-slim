<?php
require "bootstrap.php";
use Chatter\Models\User;
use Chatter\Models\Message;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging()); //one middleware

//שליפת נתונים מהטבלה המתאימה
$app->get('/users', function($request, $response, $args){
    $_user = new User();
    $users = $_user->all();
    $payload = [];
    foreach($users as $user){
        $payload[$user->id] = [
            "name" => $user->name,
            "phone" => $user->phone
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->get('/messages', function($request, $response, $args){
    $_message = new Message();
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $message){
        $payload[$message->id] = [
            "user_id" => $message->user_id,
            "body" => $message->body
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//יצירת נתונים לטבלה המתאימה
$app->post('/users', function($request, $response,$args){
    $phone = $request->getParsedBodyParam('phone','');
    $name = $request->getParsedBodyParam('name','');
    $_user = new User();
    $_user->phone = $phone;
    $_user->name = $name;
    $_user->save();
       
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $user_id = $request->getParsedBodyParam('user_id','');
    $_message = new Message();
    $_message->body = $message;
    $_message->user_id = $user_id;
    $_message->save();
       
    if($_message->id){
        $payload = ['message_id' => $_message->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//מחיקת נתונים מהטבלה המתאימה
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']);
    $message->delete();
    
    if($message->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});

//עדכון נתונים מהטבלה
$app->put('/users/{user_id}', function($request, $response,$args){
    $phone = $request->getParsedBodyParam('phone','');
    $name = $request->getParsedBodyParam('name','');
    $_user = User::find($args['user_id']);
    $_user->phone = $phone;
    $_user->name = $name;

    if($_user->save()){ //אם הערך נשמר
        $payload = ['user_id' => $_user->id,"result" => "The message has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $user_id = $request->getParsedBodyParam('user_id','');
    $_message = Message::find($args['message_id']);
    $_message->body = $message;
    $_message->user_id = $user_id;

    if($_message->save()){ //אם הערך נשמר
        $payload = ['message_id' => $_message->id,"result" => "The message has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//הוספת קבוצה לטבלה המתאימה
$app->post('/users/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody();
        
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

$app->post('/messages/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody();
        
    Message::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

//הצגת רשומה בודדת
$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = User::find($_id);
    return $response->withStatus(200)->withJson($message);
});

$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});

//אבטחת מידע
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

//JWT
/*$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => "supersecret",
    "path" => ['/messages','/users']
]));

$app->post('/auth', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user','');
    $password = $request->getParsedBodyParam('password','');

    if($user=='jack' && $password=='1234'){
        $payload = ['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6IkphY2siLCJhZG1pbiI6dHJ1ZX0.lT4jR05dBKPUqdZdehGvTScnNJGrVCqCHNg-IkeGrAU'];
        return $response->withStatus(200)->withJson($payload); 
    }
    else{
        $payload = ['token'=>null];
        return $response->withStatus(403)->withJson($payload);
    }
});*/

//jwt.io
/*{
  "sub": "1234",
  "name": "jack",
  "admin": true
}
HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
  
    supersecret

)
*/




$app->run();